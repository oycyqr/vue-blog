/*
 Navicat Premium Data Transfer

 Source Server         : 146.56.192.87
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 146.56.192.87:3306
 Source Schema         : ouyangblog

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 26/01/2021 11:56:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog_article
-- ----------------------------
DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE `blog_article`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `blog_id` int(20) NULL DEFAULT NULL COMMENT '博客ID',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文章内容txt',
  `content_html` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文章内容html',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_userId`(`blog_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章表';

-- ----------------------------
-- Records of blog_article
-- ----------------------------
BEGIN;
INSERT INTO `blog_article` VALUES (1, NULL, '数据脱敏(Data Masking),又称数据漂白、数据去隐私化或数据变形。百度百科给出的解释：数据脱敏指对某些敏感信息通过脱敏规则进行数据的变形，实现敏感隐私数据的可靠保护。在涉及客户安全数据或者一些商业性敏感数据的情况下，在不违反系统规则条件下，对真实数据进行改造并提供测试使用，如身份证号、手机号、卡号、客户号等个人信息都需要进行数据脱敏。数据安全技术之一，数据库安全技术主要包括：数据库漏扫、数据库加密、数据库防火墙、数据脱敏、数据库安全审计系统。数据库安全风险包括：拖库、刷库、撞库。开发系统', NULL), (2, NULL, '一、读写分离背景分库分表虽然可以优化数据库操作。但是要实现高并发，主从架构就应运而生了，数据库的主从复制架构，将数据库的写操作定位到主库中进行，主库和从库之间通过异步复制、半同步复制保持数据一致。所有的读操作都在主库的N个从库上进行。通过负载均衡使得每一次查询均匀的落在每一个从库上。一主n从，做读写分离（数据写入主库，通过mysql数据同步机制将主库数据同步到从库–>程序读取从库数据），多个从库之间可以实现负载均衡。次外，ShardingSphere-ShardingJdbc可手动强制部分读请求到', NULL), (3, NULL, 'ShardingSphere 数据分片(分库、分表)摘要：我们实际开发中，总有几张和业务相关的大表，这里的大表是指数据量巨大。如用户表、订单表，又或者公司业务中的主表，可能很快这种表的数据就达到了百万、千万、亿级别的规模，并且增长规模一直很快。这种情况下，单表已经满足不了了存储需求了，同时，这么大的数据量，即使搭配合理的索引，数据库查询也是很慢的，这时就需要对这些大表进行分库、分表。应用需要能对sql进行解析、改...', NULL), (4, NULL, 'MySQL 主从同步为什么要实现主从同步高并发阶段，数据库压力会非常大。然而实际上大部分的网站、 app，其实都是读多写少。针对这个情况，可以维持一个主库（数据写入），主库挂多个从库（数据读取），主库会自动把数据给同步到从库上去，一写多读，减少数据库的查询压力，从而提高并发能力。...', NULL), (5, NULL, 'Centos 7 安装与卸载MYSQL5.7一、卸载已安装的MySql1.1 rpm查看安装rpm -qa | grep -i mysql1.2rpm 卸载清除上述查找出来的MySQLrpm -e mysql57-community-release-el7-9.noarchrpm -e mysql-communi...', NULL), (6, NULL, '一、Spring Security框架1. 框架简介        官方介绍：Spring Security是一个功能强大且可高度自定义的身份验证和访问控制框架。它是保护基于Spring的应用程序的事实标准。 Spring Security是一个专注于为Java应用程序提供身份验证和授权的框架。与所有Spring项目一样，Spring Security的真正强大之处在于它可以轻松扩展以满足...', NULL), (7, NULL, '一、Spring Security框架1. 框架简介        官方介绍：Spring Security是一个功能强大且可高度自定义的身份验证和访问控制框架。它是保护基于Spring的应用程序的事实标准。 Spring Security是一个专注于为Java应用程序提供身份验证和授权的框架。与所有Spring项目一样，Spring Security的真正强大之处在于它可以轻松扩展以满足...', NULL), (8, NULL, 'SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表SpringBoot 使用JDBC连接Mysql数据库　　　　Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表', NULL), (9, NULL, '访问ElasticSearch的几种方式：REST API开源工具（Kibana、Head等）Spring Data Elasticsearch（应用程序使用接口访问或者开源框架）一、RestAPI1. 查看所有索引get：http://localhost:9200/_cat/indices?v2. ​​​​​​​创建索引put：http://localhost:9200/blogindex3.删除索引delete: http://localhost:9200/blogindex.', NULL), (10, NULL, 'ElasticSearch环境搭建：https://blog.csdn.net/u014553029/article/details/106009344SpringBoot集成ElasticSearch实战一、在pom.xml中引入依赖<!-- https://mvnrepository.com/artifact/org.springframework.data/spring-data-elasticsearch --><dependency> <gro', NULL), (11, NULL, '从零快速搭建一个SpringBoot Web项目环境：IDEA+Navicat+Jdk1.8+Mysql5.7 SpringBoot+Thymeleaf+Mybatis+SpringSecurity目录从零快速搭建一个SpringBoot Web项目 1一、新建一个SpringBoot项目 21.1 选择新增:File->New->Project 21.2 选择Spring Initializr，指定JDK版本，直接下一步 31.3填写项目相关信息，然后下一步 3', NULL), (12, NULL, '概论消息队列中间件是分布式系统中重要的组件，主要解决应用耦合，异步消息，流量 削锋等问题实现高性能，高可用，可伸缩和最终一致性[架构] 使用较多的消息队列有 ActiveMQ，RabbitMQ，ZeroMQ，Kafka，MetaMQ，RocketMQ 。消息队列在实际应用中常用的使用场景：异步处理，应用解耦，流量削锋和消息通讯四个场景。RabbitMQ是一套开源（MPL）的消息队列服务软件，是由 LShift 提供的一个 Advanced Message Queuing Protocol (AMQP)', NULL), (13, NULL, 'Centos7安装RabbitMQ一、安装Erlang由于RabbitMQ依赖Erlang， 所以需要先安装Erlang。我这次主要是从EPEL源安装Erlang。 # 启动EPEL源 $ sudo yum install epel-release # 安装erlang $ sudo yum install erlang 二、安装RabbitMQ2.1 下载RabbitMQ安装包wget http://www.rabbitmq.com/releases/rabbitmq-server', NULL), (14, NULL, 'springboot+sms 集成腾讯云短信平台实现发送短信功能一、申请短信功能到腾讯云管理平台申请短信功能（https://console.cloud.tencent.com/）1.1 创建短信签名1.2创建短信正文模板1.3 创建应用可使用系统默认应用或者创建新应用二、集成短信功能查看腾讯云账户密钥对 secretId 和 secretKey（https://console.cloud.tencent.com/cam/capi）springbo..', NULL), (15, NULL, '安装方法：File —> Settings —> Plugins—> Marketplace（搜索安装，Windows），IntellijIDEA —> Preferences… —> Plugins —> Marketplace （搜索安装，mac）强烈推荐的IDEA插件：lombokRestfulToolkitCodeGlanceAlibaba Java Coding Guidelines 和 SonarLintTranslationRedis(ie', NULL), (16, NULL, '前言在开发分布式高并发系统时有三把利器用来保护系统：缓存、降级、限流。缓存缓存的目的是提升系统访问速度和增大系统处理容量降级降级是当服务出现问题或者影响到核心流程时，需要暂时屏蔽掉，待高峰或者问题解决后再打开限流限流的目的是通过对并发访问/请求进行限速，或者对一个时间窗口内的请求进行限速来保护系统，一旦达到限制速率则可以拒绝服务、排队或等待、降级等处理本文主要讲的是api接口限流相关内容，虽然不是论述高并发概念中的限流， 不过道理都差不多。通过限流可以让系统维持在一个相对', NULL), (17, NULL, '一、环境本地：windows10服务器：centos7远程tomcat: tomcat8.5二、配置修改bin/catalina.sh文件，在最开始添加如下代码：JPDA_OPTS=-Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n然后通过以下命令启动tomcat./catalina.sh jpda startcentos7开放8000端口。三、IDEA调试启动...', NULL), (18, NULL, '一、首先我们来设置IDEA中类的模板：（IDEA中在创建类时会自动给添加注释）1、File> settings> Editor> File and Code Templates> Files我们需要设置的类模板有四个，Class、Interface、Enum、Annotation。（1）${NAME}：设置类名，与下面的${NAME}一样才能获取到创建的类名（2）TODO：代办事项的标记，一般生成类或方法都需要添加描述（3）${USER}、${DATE}、$...', NULL), (19, NULL, '摘要 有参数传递的地方都少不了参数校验。在web开发中，前端的参数校验是为了用户体验，后端的参数校验是为了安全。试想一下，如果在controller层中没有经过任何校验的参数通过service层、dao层一路来到了数据库就可能导致严重的后果，最好的结果是查不出数据，严重一点就是报错，如果这些没有被校验的参数中包含了恶意代码，那就可能导致更严重的后果。实践一、引入依赖<!--引入spring-boot-starter-validation--><depend...', NULL), (20, NULL, 'Oauth2详解: https://www.jianshu.com/p/84a4b4a1e833阮一峰的网络日志 :http://www.ruanyifeng.com/blog/2019/04/oauth-grant-types.html初识OAuth2https://blog.csdn.net/Anumbrella/article/details/99710044', NULL), (21, NULL, 'wget http://vault.centos.org/7.5.1804/os/x86_64/Packages/yum-plugin-fastestmirror-1.1.31-45.el7.noarch.rpmwget http://vault.centos.org/7.5.1804/os/x86_64/Packages/yum-metadata-parser-1.1.4-10.el7.x86_64.rpmwget http://vault.centos.org/7.5.1804/os/x86_64/', NULL), (22, NULL, 'Redis事务支持WATCHUNWATCHMULTIEXECDISCARD在一个事务的运行期间，可能会遇到两种类型的命令错误：一个命令可能会在被放入队列时失败。因此，事务有可能在调用EXEC命令之前就发生错误。例如，这个命令可能会有语法错误（参数的数量错误、命令名称错误，等等），或者可能会有某些临界条件（例如：如果使用maxmemory指令，为Redis服务器配置内存限制，那么就可能会有内存溢出条件）。在调用EXEC命令之后，事务中的某个命令可能会执行失败。例如，我们对某个键执行了错误类型', NULL), (23, NULL, 'Redis 集群的三种方案一、主从复制slave of二、哨兵模式（Sentinel）三、集群（Cluster）', NULL), (24, NULL, 'Redis 数据持久化方案一、持久化方案RDB：指定的时间间隔内保存数据快照AOF：先把命令追加到操作日志的尾部，保存所有的历史操作', NULL), (25, NULL, '一、什么是MyBatis-PlusMybatis是作为一个半自动的持久层ORM框架一直以其可以直接在XML中通过SQL语句操作数据库的灵活可控稳居持久层框架前列。但正其操作都要通过SQL语句进行，就必须写大量的xml文件，很是麻烦。而我们今天要介绍的Mybatis-Plus就是为了解决这些问题的。Mybatis-Plus是一个 Mybatis 的增强工具，在 Mybatis 的基础上只做增强不做改变，为简化开发、提高效率而生（官方定义）。它已经封装好了一些crud方法，单表的增删改查可以不用在xml中写', NULL), (26, NULL, '什么是ShiroShiro是一个强大的简单易用的Java安全框架，主要用来更便捷的认证，授权，加密，会话管理。Shiro首要的和最重要的目标就是容易使用并且容易理解，通过Shiro易于理解的API,您可以快速、轻松地获得任何应用程序——从最小的移动应用程序最大的网络和企业应用程序。Shiro架构Shiro架构图- Authentication：身份认证/登录- Authorization：验证权限，即，验证某个人是否有做某件事的权限。- Session Management:会话管理。管理', NULL), (27, NULL, '一、什么是antd design官网：https://www.antdv.com/docs/vue/introduce-cn/蚂蚁金服体验技术部经过大量的项目实践和总结，沉淀出设计语言 Ant Design。旨在统一中台项目的前端 UI 设计，屏蔽不必要的设计差异和实现成本，解放设计和前端的研发资源。目前有阿里、美团、滴滴、简书采用。Ant Design 是一个致力于提升『用户』和『设计者』使用体验的中台设计语言。它模糊了产品经理、交互设计师、视觉设计师、前端工程师、开发工程师等角色边界，将进.', NULL), (28, NULL, '概论Vue是一个单页面应用（无法通过普通的html中的<a href=\'***\'>来实现跳转），我们要实现界面切换，必须要了解什么是路由、Vue中是怎么使用路由的。一、什么是Vue-Router为了弥补Vue在开发时对路由支持的不足，官方补充了vue-router插件，它在Vue的生态环境中非常重要，在实际开发中只要编写一个页面就会操作vue-router。对于大多数单页面应用，都推荐使用官方支持的vue-router 库。更多细节可以移步vue-router 文档。官方..', NULL), (29, NULL, '一、环境准备首先我们需要在自己的开发机器上安装nodeJs，如果不确定自己是否已经安装，可以使用以下命令查看：node -v #查看node版本，检查是否安装nodenpm -vyarn -v如果正常显示版本，则说明已经安装了nodeJs，如果没有安装，可以访问node官网：http://nodejs.cn/download/，下载安装node。二、Vue CLI 全局安装安装命令：npm install -g vue-cli # -g 全局安装使用npm可能会..', NULL), (30, NULL, 'MySQL主从复制实现读写分离1. 读写分离原理2. 数据库环境准备3. master配置4. slave配置5.测试效果', NULL), (31, NULL, '一、概览1.1 什么是redis官方介绍：Redis是一个开源的、基于内存的数据结构存储器，可以用作数据库、缓存和消息中间件。总的来说Redis是一款开源的非关系型数据库，它基于内存的，key-value结构，“单线程”，支持持久化。它跟memcached类似，不过数据可以持久化，而且支持的数据类型很丰富。有字符串，链表，集 合和有序集合。1.2 redis特点支持持久化，方式包括RDB和AOF（RDB持久化----原理是将Reids在内存中的数据库记录定时dump到磁盘上的RDB持久化', NULL), (32, NULL, 'SpringBoot + Redis 实现缓存一、在pom中添加依赖<!--springboot redis依赖--><dependency> <groupId>org.springframework.boot</groupId> <artifactId>spring-boot-starter-data-redis</artifactId></depen...', NULL), (33, NULL, 'ELK--Elasticsearch 安装 ik分词器 插件分词把一段文字的划分成一个个的关键字，我们在搜索时候会把自己的信息进行分词，会把数据库中或者索引库中的数据进行分词，然后进行一个匹配操作。默认的中文分词是将每个字看成一个词，这显然是不符合要求的，所以我们需要安装中文分词器ik来解决这个问题。Elasticsearch内置分词器 Standard - 默认分词器，按词切分，小写处理 Simple - 按照非字母...', NULL), (34, NULL, 'ELK--Logstash 安装中国镜像：https://www.newbe.pro/Mirrors/Mirrors-Logstash/ https://mirrors.huaweicloud.com/kibana/?C=N&O=D1.下载logstash安装包wget https://mirrors.huaweicloud.com/logstash/6.4.1/logstash-6.4.1...', NULL), (35, NULL, '1.下载解压安装包，一定要装与ES相同的版本下载地址：https://www.elastic.co/downloads/kibana(国内镜像地址：https://www.newbe.pro/Mirrors/Mirrors-Kibana/)下载安装包：# 下载：wget https://mirrors.huaweicloud.com/kibana/6.4.1/kibana-6.4.1-linux-x86_64.tar.gz# 解压tar -xvf kibana-6.4.1-li...', NULL), (36, NULL, 'Elasticsearch 入门到精通--Elasticsearch安装1.下载安装包wgethttps://elasticsearch.thans.cn/downloads/elasticsearch/elasticsearch-6.4.1.tar.gz由于官网下载比较慢，这里使用了Elasticsearch 国内镜像下载站，其他版本请查看：https://thans.cn/mirror/elasticsearch.html2.解压安装包...', NULL), (37, NULL, '在平时的java开发工作中，特别是将应用部署到服务器之后，经常会出现各种各样的问题，例如内存泄漏、死锁、CPU飙高等。下面我们就来学习掌握一些工具来分析到底是哪里出了问题，做到及时定位问题、有效解决问题。', NULL), (38, NULL, '一、什么是Swagger 由于Spring Boot能够快速开发、便捷部署等特性，相信有很大一部分Spring Boot的用户会用来构建RESTful API。由于存在多终端的情况（移动端，web前端，小程序等），所以我们会抽象出RESTful API并共用一些底层业务代码。 由于接口众多，并且细节复杂，所以催生了一些api框架，Swagger就凭借其使用简单、...', NULL), (39, NULL, '1、确定Java应用进程编号使用jps或ps -ef|grep java命令确定想要分析的应用的进程编号。2、使用死锁检测工具检测死锁2.1 Jstack命令jstack是java虚拟机自带的一种堆栈跟踪工具。jstack用于打印出给定的java进程ID或core file或远程调试服务的Java堆栈信息。 Jstack工具可以用于生成java虚拟机当前时刻的', NULL);
COMMIT;

-- ----------------------------
-- Table structure for blog_article_info
-- ----------------------------
DROP TABLE IF EXISTS `blog_article_info`;
CREATE TABLE `blog_article_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者昵称',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章标题',
  `summary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章摘要',
  `category_id` int(11) NULL DEFAULT NULL COMMENT '文章分类ID',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `thumbnail` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `sort` int(255) NULL DEFAULT NULL COMMENT '排序',
  `view_num` int(11) NULL DEFAULT NULL COMMENT '浏览数',
  `click_count` int(255) NULL DEFAULT NULL COMMENT '点击量',
  `collect_count` int(255) NULL DEFAULT NULL COMMENT '收藏量',
  `love_count` int(255) NULL DEFAULT NULL COMMENT '喜欢量',
  `level` int(1) NULL DEFAULT 0 COMMENT '等级（1：banner 置顶 ）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_category_id`(`category_id`) USING BTREE,
  INDEX `key_userId`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章表';

-- ----------------------------
-- Records of blog_article_info
-- ----------------------------
BEGIN;
INSERT INTO `blog_article_info` VALUES (1, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 21, 1, 100, 99, 101, 0), (2, 1, '乾源', 'ShardingSphere-ShardingJdbc 读写分离', 'ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1909, 1, 100, 99, 101, 0), (3, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据分片(分库、分表)', 'ShardingSphere-ShardingJdbc 数据分片(分库、分表)ShardingSphere-ShardingJdbc 数据分片(分库、分表)ShardingSphere-ShardingJdbc 数据分片(分库、分表)', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 111, 1, 100, 99, 101, 0), (4, 1, '乾源', 'MySQL 主从同步原理与实践', 'MySQL 主从同步原理与实践MySQL 主从同步原理与实践MySQL 主从同步原理与实践', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (5, 1, '乾源', 'Centos 7 MYSQL5.7 安装与卸载', 'Centos 7 MYSQL5.7 安装与卸载Centos 7 MYSQL5.7 安装与卸载Centos 7 MYSQL5.7 安装与卸载', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (6, 1, '乾源', 'SpringBoot+SpringSecurity+mysql实现认证与授权', 'SpringBoot+SpringSecurity+mysql实现认证与授权SpringBoot+SpringSecurity+mysql实现认证与授权SpringBoot+SpringSecurity+mysql实现认证与授权', 4, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner/banner1.jpg', 1, 1, 100, 99, 101, 1), (7, 1, '乾源', 'SpringBoot+Spring Security基于内存用户认证', 'SpringBoot+Spring Security基于内存用户认证SpringBoot+Spring Security基于内存用户认证SpringBoot+Spring Security基于内存用户认证', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (8, 1, '乾源', 'SpringBoot +JDBC连接Mysql数据库', 'Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表', 4, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner/banner2.jpg', 1, 1, 100, 99, 101, 1), (9, 1, '乾源', '访问ElasticSearch的几种方式', '访问ElasticSearch的几种方式访问ElasticSearch的几种方式访问ElasticSearch的几种方式', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (10, 1, '乾源', 'SpringBoot+ElasticSearch 实现全文检索', 'SpringBoot+ElasticSearch 实现全文检索SpringBoot+ElasticSearch 实现全文检索SpringBoot+ElasticSearch 实现全文检索', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (11, 1, '乾源', '快速从零搭建一个SpringBoot Web项目', '快速从零搭建一个SpringBoot Web项目快速从零搭建一个SpringBoot Web项目快速从零搭建一个SpringBoot Web项目', 4, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner/banner3.jpg', 1, 1, 100, 99, 101, 1), (12, 1, '乾源', 'SpringBoot+RabbitMQ 实现消息队列', 'SpringBoot+RabbitMQ 实现消息队列SpringBoot+RabbitMQ 实现消息队列SpringBoot+RabbitMQ 实现消息队列', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (13, 1, '乾源', '超简单的Centos7安装RabbitMQ教程', '超简单的Centos7安装RabbitMQ教程超简单的Centos7安装RabbitMQ教程超简单的Centos7安装RabbitMQ教程', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner/banner5.jpg', 1, 1, 100, 99, 101, 1), (14, 1, '乾源', 'springboot+sms 集成腾讯云短信平台', 'springboot+sms 集成腾讯云短信平台springboot+sms 集成腾讯云短信平台springboot+sms 集成腾讯云短信平台', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (15, 1, '乾源', '强烈推荐的IDEA插件', '强烈推荐的IDEA插件强烈推荐的IDEA插件强烈推荐的IDEA插件', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (16, 1, '乾源', 'Springboot+Redis 实现API接口防刷限流', 'Springboot+Redis 实现API接口防刷限流Springboot+Redis 实现API接口防刷限流Springboot+Redis 实现API接口防刷限流', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (17, 1, '乾源', 'IDEA开启Tomcat远程调试', 'IDEA开启Tomcat远程调试IDEA开启Tomcat远程调试IDEA开启Tomcat远程调试', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (18, 1, '乾源', 'IDEA Java代码注释规范', 'IDEA Java代码注释规范IDEA Java代码注释规范IDEA Java代码注释规范', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner/banner4.jpg', 1, 1, 100, 99, 101, 1), (19, 1, '乾源', 'SpringBoot + validation 接口参数校验', 'SpringBoot + validation 接口参数校验SpringBoot + validation 接口参数校验SpringBoot + validation 接口参数校验', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (20, 1, '乾源', 'OAuth 2.0', 'OAuth 2.0OAuth 2.0OAuth 2.0', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (21, 1, '乾源', 'Linux删除系统自带版本Python过程详解', 'Linux删除系统自带版本Python过程详解Linux删除系统自带版本Python过程详解Linux删除系统自带版本Python过程详解', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (22, 1, '乾源', 'Redis事务支持', 'Redis事务支持Redis事务支持Redis事务支持', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (23, 1, '乾源', 'Redis 集群的三种方案', 'Redis 集群的三种方案Redis 集群的三种方案Redis 集群的三种方案', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (24, 1, '乾源', 'Redis 数据持久化方案', 'Redis 数据持久化方案Redis 数据持久化方案Redis 数据持久化方案', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner.jpg', 1, 1, 100, 99, 101, 0), (25, 1, '乾源', 'SpringBoot 整合MyBatis-Plus', 'SpringBoot 整合MyBatis-PlusSpringBoot 整合MyBatis-PlusSpringBoot 整合MyBatis-Plus', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (26, 1, '乾源', 'SpringBoot整合Shiro安全框架', 'SpringBoot整合Shiro安全框架SpringBoot整合Shiro安全框架SpringBoot整合Shiro安全框架', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 1), (27, 1, '乾源', 'SpringBoot + Vue 搭建 Blog（三）-- 集成antd design', 'SpringBoot + Vue 搭建 Blog（三）-- 集成antd designSpringBoot + Vue 搭建 Blog（三）-- 集成antd designSpringBoot + Vue 搭建 Blog（三）-- 集成antd design', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (28, 1, '乾源', 'SpringBoot + Vue 搭建 Blog（二）-- Vue-Router入门', 'SpringBoot + Vue 搭建 Blog（二）-- Vue-Router入门SpringBoot + Vue 搭建 Blog（二）-- Vue-Router入门SpringBoot + Vue 搭建 Blog（二）-- Vue-Router入门', 2, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (29, 1, '乾源', 'SpringBoot + Vue 搭建 Blog（一）-- 安装 vue-cli 脚手架', 'SpringBoot + Vue 搭建 Blog（一）-- 安装 vue-cli 脚手架SpringBoot + Vue 搭建 Blog（一）-- 安装 vue-cli 脚手架SpringBoot + Vue 搭建 Blog（一）-- 安装 vue-cli 脚手架', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (30, 1, '乾源', 'MySQL主从复制实现读写分离', 'MySQL主从复制实现读写分离MySQL主从复制实现读写分离MySQL主从复制实现读写分离', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (31, 1, '乾源', '世界上最简单的 Redis 介绍与基础操作', '世界上最简单的 Redis 介绍与基础操作世界上最简单的 Redis 介绍与基础操作世界上最简单的 Redis 介绍与基础操作', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner.jpg', 1, 1, 100, 99, 101, 0), (32, 1, '乾源', 'SpringBoot + Redis 实现缓存', 'SpringBoot + Redis 实现缓存SpringBoot + Redis 实现缓存SpringBoot + Redis 实现缓存', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (33, 1, '乾源', 'ELK--Elasticsearch 安装 ik分词器 插件', 'ELK--Elasticsearch 安装 ik分词器 插件ELK--Elasticsearch 安装 ik分词器 插件ELK--Elasticsearch 安装 ik分词器 插件', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 1), (34, 1, '乾源', 'ELK--Logstash 安装', 'ELK--Logstash 安装ELK--Logstash 安装ELK--Logstash 安装', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (35, 1, '乾源', 'ELK--kibana 安装', 'ELK--kibana 安装ELK--kibana 安装ELK--kibana 安装', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (36, 1, '乾源', 'ELK--Elasticsearch安装', 'ELK--Elasticsearch安装ELK--Elasticsearch安装ELK--Elasticsearch安装', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (37, 1, '乾源', 'jvm分析&调优', 'jvm分析&调优jvm分析&调优jvm分析&调优', 3, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/banner.jpg', 1, 1, 100, 99, 101, 0), (38, 1, '乾源', 'Spring Boot + Swagger2 自动生成api接口文档', 'Spring Boot + Swagger2 自动生成api接口文档Spring Boot + Swagger2 自动生成api接口文档Spring Boot + Swagger2 自动生成api接口文档', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (39, 1, '乾源', 'Java死锁和Java进程Java CPU 100%排查', 'Java死锁和Java进程Java CPU 100%排查Java死锁和Java进程Java CPU 100%排查Java死锁和Java进程Java CPU 100%排查', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, 1, 100, 99, 101, 0), (40, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (41, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (42, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (43, 1, '乾源', 'ShardingSphere-ShardingJdbc 读写分离', 'ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (44, 1, '乾源', 'ShardingSphere-ShardingJdbc 读写分离', 'ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 190, NULL, 100, 99, 101, 0), (45, 1, '乾源', 'ShardingSphere-ShardingJdbc 读写分离', 'ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离ShardingSphere-ShardingJdbc 读写分离', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1909, NULL, 100, 99, 101, 0), (46, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (47, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (48, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (49, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (50, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (51, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1233141234, NULL, 100, 99, 101, 0), (52, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 1, NULL, 100, 99, 101, 0), (53, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (54, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (55, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (56, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 122, NULL, 100, 99, 101, 0), (57, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (58, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 11, '/static/img/cover.jpg', 11, NULL, 100, 99, 101, 0), (59, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据脱敏', 'ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏ShardingSphere-ShardingJdbc 数据脱敏', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 1145, NULL, 100, 99, 101, 0), (60, 1, '乾源', 'ShardingSphere-ShardingJdbc 数据分片(分库、分表)', 'ShardingSphere-ShardingJdbc 数据分片(分库、分表)ShardingSphere-ShardingJdbc 数据分片(分库、分表)ShardingSphere-ShardingJdbc 数据分片(分库、分表)', 1, '2021-01-22 10:59:26', '2021-01-22 10:59:26', 1, '/static/img/cover.jpg', 111, NULL, 100, 99, 101, 0);
COMMIT;

-- ----------------------------
-- Table structure for blog_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `blog_article_tag`;
CREATE TABLE `blog_article_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `article_id` bigint(20) NOT NULL COMMENT '文章ID',
  `tag_id` int(11) NOT NULL COMMENT '标签ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_articleId`(`article_id`) USING BTREE,
  INDEX `key_tagId`(`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章标签表对应表';

-- ----------------------------
-- Records of blog_article_tag
-- ----------------------------
BEGIN;
INSERT INTO `blog_article_tag` VALUES (1, 1, 1, '2020-12-16 22:47:05', '2020-12-16 22:47:09'), (2, 1, 2, '2020-12-16 22:47:24', '2020-12-16 22:47:29');
COMMIT;

-- ----------------------------
-- Table structure for blog_category
-- ----------------------------
DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE `blog_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别名字',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别图标',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类描述',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章类别表';

-- ----------------------------
-- Records of blog_category
-- ----------------------------
BEGIN;
INSERT INTO `blog_category` VALUES (1, '后端技术', '/static/img/logo.jpg', '我的博客分类之- 后端技术 相关文章', '2020-12-16 22:44:54', '2020-12-16 22:44:54', 1, 1), (2, '前端技术', '/static/img/logo.jpg', '我的博客分类之- 前端技术 相关文章', '2020-12-16 22:44:54', '2020-12-16 22:44:54', 1, 1), (3, '数据库', '/static/img/logo.jpg', '我的博客分类之- 数据库 相关文章', '2020-12-16 22:44:54', '2020-12-16 22:44:54', 1, 1), (4, '中间件', '/static/img/logo.jpg', '我的博客分类之- 中间件 相关文章', '2020-12-16 22:44:54', '2020-12-16 22:44:54', 1, 1), (5, '生活杂谈', '/static/img/logo.jpg', '我的博客分类之- 生活杂谈 相关文章', '2020-12-16 22:44:54', '2020-12-16 22:44:54', 1, 1), (6, '百事百科', '/static/img/logo.jpg', '我的博客分类之- 百事百科 相关文章', '2020-12-16 22:54:02', '2020-12-16 22:54:06', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for blog_comment
-- ----------------------------
DROP TABLE IF EXISTS `blog_comment`;
CREATE TABLE `blog_comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `article_id` bigint(20) NULL DEFAULT NULL COMMENT '文章ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父评论Id',
  `to_uid` bigint(20) NULL DEFAULT NULL COMMENT '评论的评论用户ID',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_articleId`(`article_id`) USING BTREE,
  INDEX `key_parentId`(`parent_id`) USING BTREE,
  INDEX `key_toUserId`(`to_uid`) USING BTREE,
  INDEX `key_userId`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '评论表';

-- ----------------------------
-- Records of blog_comment
-- ----------------------------
BEGIN;
INSERT INTO `blog_comment` VALUES (1, 1, 1, 'Spring连接Mysql的方式有很多，例如JDBC，Spring JPA，Hibeirnate，Mybatis等，本文主要介绍使用最简单、最底层的JDBC方式来连接Mysql数据库，JDBC连接数据库，主要是注入JdbcTemplate，使用JdbcTemplate来操作数据库。一、在mysql中的test库中建立user表', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (2, 1, 1, '写得好1', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (3, 1, 1, '写得好2', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (4, 1, 1, '写得好3', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (5, 1, 1, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (6, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (7, 1, 3, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (8, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (9, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (10, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (11, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (12, 1, 2, '写得好4', NULL, NULL, '2020-12-16 22:52:01', '2020-12-16 22:52:06');
COMMIT;

-- ----------------------------
-- Table structure for blog_link
-- ----------------------------
DROP TABLE IF EXISTS `blog_link`;
CREATE TABLE `blog_link`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题、名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `status` tinyint(20) NULL DEFAULT NULL COMMENT '状态',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_articleId`(`url`) USING BTREE,
  INDEX `key_toUserId`(`status`) USING BTREE,
  INDEX `key_userId`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '评论表';

-- ----------------------------
-- Records of blog_link
-- ----------------------------
BEGIN;
INSERT INTO `blog_link` VALUES (1, '1link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (2, '2link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (3, '3link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (4, '4link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (5, '5link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (6, '6link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (7, '7link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (8, '8link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (9, '9link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (10, '10link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (11, '11link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06'), (12, '12link 链接', 'http://wwww.baidu.com', '/static/img/logo.jpg', 1, '2020-12-16 22:52:01', '2020-12-16 22:52:06');
COMMIT;

-- ----------------------------
-- Table structure for blog_log
-- ----------------------------
DROP TABLE IF EXISTS `blog_log`;
CREATE TABLE `blog_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问Ip',
  `module` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作模块',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问方法',
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法参数',
  `nickname` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人昵称',
  `operation` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作事项',
  `time` bigint(20) NULL DEFAULT NULL COMMENT '操作耗时',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '操作用户userId',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户操作日志表';

-- ----------------------------
-- Table structure for blog_menu
-- ----------------------------
DROP TABLE IF EXISTS `blog_menu`;
CREATE TABLE `blog_menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表';

-- ----------------------------
-- Table structure for blog_permission
-- ----------------------------
DROP TABLE IF EXISTS `blog_permission`;
CREATE TABLE `blog_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id\r\n',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限',
  `creater` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表';

-- ----------------------------
-- Table structure for blog_role
-- ----------------------------
DROP TABLE IF EXISTS `blog_role`;
CREATE TABLE `blog_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表';

-- ----------------------------
-- Records of blog_role
-- ----------------------------
BEGIN;
INSERT INTO `blog_role` VALUES (1, 'ROLE_ADMIN', 'admin', NULL, 1, '1', '1', '0', '', NULL, '', NULL, NULL), (2, 'ROLE_USER', 'user', NULL, 2, '1', '1', '0', '', NULL, '', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for blog_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `blog_role_menu`;
CREATE TABLE `blog_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系';

-- ----------------------------
-- Table structure for blog_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `blog_role_permission`;
CREATE TABLE `blog_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `permission_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限对应关系';

-- ----------------------------
-- Table structure for blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `blog_tag`;
CREATE TABLE `blog_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `tag_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签名字',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签图标',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签颜色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '标签表';

-- ----------------------------
-- Records of blog_tag
-- ----------------------------
BEGIN;
INSERT INTO `blog_tag` VALUES (1, 1, 'SpringBoot', NULL, '2020-12-16 22:39:40', '2020-12-16 22:39:46', 1, '#2db7f5'), (2, 1, 'MyBatis', NULL, '2020-12-16 22:39:40', '2020-12-16 22:39:40', 1, '#87d068'), (3, 1, 'SpringSecurity', NULL, '2020-12-16 22:39:40', '2020-12-16 22:39:40', 1, '#108ee9'), (4, 1, 'MySql', NULL, '2020-12-16 22:39:40', '2020-12-16 22:39:40', 1, '#2db7f5'), (5, 1, 'Redis', NULL, '2020-12-16 22:39:40', '2020-12-16 22:39:40', 1, '#87d068'), (6, 1, 'SpringCloud', NULL, '2020-12-16 22:48:21', '2020-12-16 22:48:25', 1, '#108ee9');
COMMIT;

-- ----------------------------
-- Table structure for blog_user
-- ----------------------------
DROP TABLE IF EXISTS `blog_user`;
CREATE TABLE `blog_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `last_login_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登录时间',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `admin` tinyint(1) NULL DEFAULT 0 COMMENT '是否admin，0不是，1是',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '用户状态',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_account`(`account`) USING BTREE,
  UNIQUE INDEX `idx_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表';

-- ----------------------------
-- Records of blog_user
-- ----------------------------
BEGIN;
INSERT INTO `blog_user` VALUES (1, 'oyc', 'e10adc3949ba59abbe56e057f20f883e', '111', '1456682842@qq.com', '17576019421', '2020-12-16 10:05:36', '乾源', NULL, 1, 1, '2020-12-16 10:05:59', '2020-12-16 10:06:01'), (2, 'oyc2', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc2@qq.com', '17576019421', '2020-12-16 10:19:22', '乾源', 'null', 1, 1, '2020-12-16 10:19:22', '2020-12-16 10:19:22'), (3, 'oyc3', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc3@qq.com', '17576019421', '2020-12-16 10:21:05', '乾源', 'null', 1, 1, '2020-12-16 10:21:05', '2020-12-16 10:21:05'), (4, 'oyc4', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc4@qq.com', '17576019421', '2020-12-16 10:21:18', '乾源', 'null', 1, 1, '2020-12-16 10:21:18', '2020-12-16 10:21:18'), (10, 'oyc10', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc10@qq.com', '17576019421', '2020-12-16 10:21:25', '乾源', 'null', 1, 1, '2020-12-16 10:21:25', '2020-12-16 10:21:25'), (11, 'oyc11', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc11@qq.com', '17576019421', '2020-12-16 10:22:03', '乾源', 'null', 1, 1, '2020-12-16 10:22:03', '2020-12-16 10:22:03'), (12, 'oyc12', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc12@qq.com', '17576019421', '2020-12-16 10:22:21', '乾源', 'null', 1, 1, '2020-12-16 10:22:21', '2020-12-16 10:22:21'), (15, 'oyc6', 'e10adc3949ba59abbe56e057f20f883e', '111', '1456682841@qq.com', '17576019421', '2020-12-16 10:05:36', '乾源', NULL, 1, 1, '2020-12-16 10:05:36', '2020-12-16 10:05:36'), (16, 'lisi', 'e10adc3949ba59abbe56e057f20f883e', '11', 'lisi@163.com', '13700009999', '2020-12-16 15:52:19', '李四', NULL, 1, 1, '2020-12-16 15:52:19', '2020-12-16 15:52:19'), (19, 'oyc112', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc112@qq.com', '17576019421', '2020-12-16 10:19:22', '乾源', 'null', 1, 1, '2020-12-16 10:19:22', '2020-12-16 10:19:22'), (20, '1', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc113@qq.com', '17576019421', '2020-12-16 10:21:05', '乾源', 'null', 1, 1, '2020-12-16 10:21:05', '2020-12-16 10:21:05'), (21, 'oyc41', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc114@qq.com', '17576019421', '2020-12-16 10:21:18', '乾源', 'null', 1, 1, '2020-12-16 10:21:18', '2020-12-16 10:21:18'), (22, 'oyc110', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc11110@qq.com', '17576019421', '2020-12-16 10:21:25', '乾源', 'null', 1, 1, '2020-12-16 10:21:25', '2020-12-16 10:21:25'), (23, 'oyc111', 'e10adc3949ba59abbe56e057f20f883e', '111', 'oyc11111@qq.com', '17576019421', '2020-12-16 10:22:03', '乾源', 'null', 1, 1, '2020-12-16 10:22:03', '2020-12-16 10:22:03');
COMMIT;

-- ----------------------------
-- Table structure for blog_user_role
-- ----------------------------
DROP TABLE IF EXISTS `blog_user_role`;
CREATE TABLE `blog_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系';

-- ----------------------------
-- Records of blog_user_role
-- ----------------------------
BEGIN;
INSERT INTO `blog_user_role` VALUES (1, 1, 1), (2, 1, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_service_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_service_log`;
CREATE TABLE `sys_service_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `spend_time` int(11) NULL DEFAULT NULL COMMENT '请求耗时',
  `request_time` datetime(0) NOT NULL COMMENT '请求时间',
  `response_time` datetime(0) NULL DEFAULT NULL COMMENT '响应时间',
  `success_status` int(1) NOT NULL COMMENT '是否成功（0失败 1成功）',
  `api_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口名称',
  `request_message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `response_message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回报文',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23041272 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'API日志表';

-- ----------------------------
-- Records of sys_service_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_service_log` VALUES (23041244, '219.137.250.148', 518, '2020-11-11 10:51:10', '2020-11-11 10:51:10', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":58,\"timestamp\":null}', NULL), (23041245, '219.137.250.148', 416, '2020-11-11 10:51:10', '2020-11-11 10:51:11', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":59,\"timestamp\":null}', NULL), (23041246, '219.137.250.148', 443, '2020-11-11 10:51:11', '2020-11-11 10:51:11', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":60,\"timestamp\":null}', NULL), (23041247, '219.137.250.148', 415, '2020-11-11 10:51:12', '2020-11-11 10:51:12', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":61,\"timestamp\":null}', NULL), (23041248, '219.137.250.148', 387, '2020-11-11 10:51:12', '2020-11-11 10:51:13', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":62,\"timestamp\":null}', NULL), (23041249, '219.137.250.148', 383, '2020-11-11 10:51:13', '2020-11-11 10:51:13', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":63,\"timestamp\":null}', NULL), (23041250, '219.137.250.148', 383, '2020-11-11 10:51:13', '2020-11-11 10:51:14', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":64,\"timestamp\":null}', NULL), (23041251, '172.16.3.71', 4843, '2020-11-11 10:46:26', '2020-11-11 10:46:30', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":100,\"userName\":null,\"type\":null,\"pageNum\":0,\"timestamp\":null}', NULL), (23041252, '219.137.250.148', 340, '2020-11-11 10:51:14', '2020-11-11 10:51:14', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":65,\"timestamp\":null}', NULL), (23041253, '219.137.250.148', 384, '2020-11-11 10:51:15', '2020-11-11 10:51:15', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":66,\"timestamp\":null}', NULL), (23041254, '219.137.250.148', 578, '2020-11-11 10:51:15', '2020-11-11 10:51:16', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":67,\"timestamp\":null}', NULL), (23041255, '219.137.250.148', 362, '2020-11-11 10:51:16', '2020-11-11 10:51:16', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":68,\"timestamp\":null}', NULL), (23041256, '219.137.250.148', 334, '2020-11-11 10:51:17', '2020-11-11 10:51:17', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":69,\"timestamp\":null}', NULL), (23041257, '219.137.250.148', 427, '2020-11-11 10:51:17', '2020-11-11 10:51:18', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":70,\"timestamp\":null}', NULL), (23041258, '219.137.250.148', 464, '2020-11-11 10:51:18', '2020-11-11 10:51:18', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":71,\"timestamp\":null}', NULL), (23041259, '219.137.250.148', 457, '2020-11-11 10:51:19', '2020-11-11 10:51:19', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":72,\"timestamp\":null}', NULL), (23041260, '172.16.3.71', 4669, '2020-11-11 10:46:31', '2020-11-11 10:46:35', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":100,\"userName\":null,\"type\":null,\"pageNum\":1,\"timestamp\":null}', NULL), (23041261, '219.137.250.148', 856, '2020-11-11 10:51:19', '2020-11-11 10:51:20', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":73,\"timestamp\":null}', NULL), (23041262, '219.137.250.148', 381, '2020-11-11 10:51:20', '2020-11-11 10:51:20', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":74,\"timestamp\":null}', NULL), (23041263, '219.137.250.148', 375, '2020-11-11 10:51:21', '2020-11-11 10:51:21', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":75,\"timestamp\":null}', NULL), (23041264, '219.137.250.148', 380, '2020-11-11 10:51:21', '2020-11-11 10:51:22', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":76,\"timestamp\":null}', NULL), (23041265, '219.137.250.148', 590, '2020-11-11 10:51:22', '2020-11-11 10:51:22', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":77,\"timestamp\":null}', NULL), (23041266, '219.137.250.148', 381, '2020-11-11 10:51:22', '2020-11-11 10:51:23', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":78,\"timestamp\":null}', NULL), (23041267, '219.137.250.148', 386, '2020-11-11 10:51:23', '2020-11-11 10:51:23', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":79,\"timestamp\":null}', NULL), (23041268, '219.137.250.148', 408, '2020-11-11 10:51:24', '2020-11-11 10:51:24', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":80,\"timestamp\":null}', NULL), (23041269, '172.16.3.71', 4935, '2020-11-11 10:46:35', '2020-11-11 10:46:40', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":100,\"userName\":null,\"type\":null,\"pageNum\":2,\"timestamp\":null}', NULL), (23041270, '219.137.250.148', 339, '2020-11-11 10:51:24', '2020-11-11 10:51:24', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":81,\"timestamp\":null}', NULL), (23041271, '219.137.250.148', 386, '2020-11-11 10:51:25', '2020-11-11 10:51:25', 1, 'userList', '{\"no\":null,\"loginName\":null,\"pageSize\":10,\"userName\":null,\"type\":1,\"pageNum\":82,\"timestamp\":null}', NULL);
COMMIT;

-- ----------------------------
-- Table structure for to_blog_user_token
-- ----------------------------
DROP TABLE IF EXISTS `to_blog_user_token`;
CREATE TABLE `to_blog_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE,
  INDEX `key_userId`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户Token';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, '欧阳城', 'ouyangcheng', '123456'), (2, '余秋容', 'yuqiurong', '123456'), (3, '欧阳城', 'ouyangcheng', '123456'), (4, '欧阳城', 'ouyangcheng', '123456');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
