package com.oycbest.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oycbest.blog.entity.BlogUser;

import java.util.List;

/**
 * 用户表(BlogUser)表服务接口
 *
 * @author oyc
 * @since 2020-12-16 11:17:10
 */
public interface BlogUserService extends IService<BlogUser> {
    /**
     * 删除（根据ID 批量删除）
     *
     * @param ids 主键ID列表
     */
    void updateStatus(String[] ids, int status);

    /**
     * 根据角色id获取用户列表
     * @param roleId 角色id
     */
    List<BlogUser> selectByRoleId(int roleId);
}
