package com.oycbest.blog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章表(BlogArticleInfo)表实体类
 *
 * @author oyc
 * @since 2020-12-16 11:29:06
 */
@SuppressWarnings("serial")
@Data
public class BlogArticleInfo extends Model<BlogArticleInfo> {

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 作者昵称
     */
    private String author;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 文章摘要
     */
    private String summary;

    /**
     * 文章分类ID
     */
    private Integer categoryId;

    /**
     * 等级
     */
    private Integer level;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 状态
     */
    private Object status;

    private String thumbnail;

    /**
     *  排序
     */
    private Integer sort;

    /**
     * 点击量
     */
    private Integer clickCount;

    /**
     * 收藏量
     */
    private Integer collectCount;

    /**
     * 点赞量
     */
    private Integer loveCount;

    /**
     * 文章内容 -- 对应BlogArticle中的content
     */
    @TableField(exist = false)
    private String blogContent;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
