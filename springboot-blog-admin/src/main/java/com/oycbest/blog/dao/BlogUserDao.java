package com.oycbest.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oycbest.blog.entity.BlogUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表(BlogUser)表数据库访问层
 *
 * @author oyc
 * @since 2020-12-16 11:17:10
 */
public interface BlogUserDao extends BaseMapper<BlogUser> {
    /**
     * 更新状态
     * @param ids 用户idiebook
     * @param status 状态
     */
    void updateStatus(@Param("idList")String[] ids,@Param("status") int status);

    /**
     * 根据角色id获取用户列表
     * @param roleId 角色id
     * @return
     */
    List<BlogUser> selectByRoleId(@Param("roleId")int roleId);
}
