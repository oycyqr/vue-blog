package com.oycbest.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oycbest.blog.entity.BlogUserRole;
import org.apache.ibatis.annotations.Param;

/**
 * 用户与角色对应关系(BlogUserRole)表数据库访问层
 *
 * @author oyc
 * @since 2020-12-16 11:17:12
 */
public interface BlogUserRoleDao extends BaseMapper<BlogUserRole> {
    /**
     * 插入用户角色管理
     *
     * @param roleId  角色id
     * @param userIds 用户id
     */
    void insertAssociated(@Param("roleId") int roleId, @Param("userIds") String[] userIds);
}
