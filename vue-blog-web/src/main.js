// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue' // 引入Vue
import App from './App'
import router from './router'
import ant from 'ant-design-vue' // 引入Antd组件
import axios from 'axios'
// 引入样式
import 'ant-design-vue/dist/antd.css'
import "@/assets/iconfont/iconfont.css";
import '@/assets/css/index.css';
import '@/assets/css/common.css';

// 引入公共JS
/*import showBlogDetailBlank from './utils/util.js';*/

// 图片查看
import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
// Vue.use(preview)
import 'vue-photo-preview/dist/skin.css'
var option = {
  maxSpreadZoom: 1, // 控制预览图最大的倍数，默认是2倍，我这里改成了原图
  fullscreenEl: false, //控制是否显示右上角全屏按钮
  closeEl: true, //控制是否显示右上角关闭按钮
  tapToClose: true, //点击滑动区域应关闭图库
  shareEl: false, //控制是否显示分享按钮
  zoomEl: false, //控制是否显示放大缩小按钮
  counterEl: false, //控制是否显示左上角图片数量按钮
  arrowEl: true,  //控制如图的左右箭头（pc浏览器模拟手机时）
  tapToToggleControls: true, //点击应切换控件的可见性
  clickToCloseNonZoomable: true //点击图片应关闭图库，仅当图像小于视口的大小时
}
Vue.use(preview, option)

//图片预览 -- viewer
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

Vue.use(Viewer)
Viewer.setDefaults({
  Options: {
    'inline': true,
    'button': true,
    'navbar': true,
    'title': true,
    'toolbar': true,
    'tooltip': true,
    'movable': true,
    'zoomable': true,
    'rotatable': true,
    'scalable': true,
    'transition': true,
    'fullscreen': true,
    'keyboard': true,
    'url': 'data-source'
  }
})


// 使用Antd
Vue.use(ant)
Vue.prototype.$http = axios;

//使用 FormModel
import { FormModel } from 'ant-design-vue'
Vue.use(FormModel)

// 生产环境提示，这里设置成了false
Vue.config.productionTip = false

Vue.prototype.$axios = axios
// 每次发送的请求都会带一个/api的前缀
axios.defaults.baseURL = '/api'
Vue.config.productionTip = false
axios.defaults.timeout = 5000

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
Vue.use(mavonEditor)

import VueHighlightJS from 'vue-highlightjs'
import 'highlight.js/styles/atom-one-dark.css'
Vue.use(VueHighlightJS)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
