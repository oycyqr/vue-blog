import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld' // 引入根目录下的 HelloWorld.vue 组件
import HomeIndex from '@/views/index/main'

Vue.use(Router)

//获取原型对象上的push函数
const originalPush = Router.prototype.push
//修改原型对象中的push方法
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export const constantRouterMap = [
  /*前端页面*/
  {
    path: '/',
    component: HomeIndex,
    children: [
      {path: '/', component: () => import('@/views/index/index')},
      {path: '/category', component: () => import('@/views/index/category')},
      {path: '/category/:categoryId', component: () => import('@/views/index/category')},
      {path: '/main/list', component: () => import('@/views/index/list')},
      {path: '/detail', component: () => import('@/views/index/detail')},
      {path: '/detail/:blogId', component: () => import('@/views/index/detail')}
    ]
  },
  /*异常页面*/
  {path: '/404', component: () => import('@/views/error/404')},
  {path: '/!*', component: () => import('@/views/error/404')},
  /*{path: '/500', component: () => import('@/views/error/500')},
  {path: '/502', component: () => import('@/views/error/502')},
  */

  /*后台管理*/
  {
    path: '/admin',
    name: 'admin',
    component: () => import('@/views/admin/Admin'),
    children: [
      {path: '/admin/user', component: () => import('@/views/admin/UserList')},
      //{path: '/admin/menu', component: () => import('@/views/admin/menu')},
      {path: '/admin/role', component: () => import('@/views/admin/RoleList')},
      {path: '/admin/category', component: () => import('@/views/admin/CategoryList')},
      {path: '/admin/blog', component: () => import('@/views/admin/BlogList')},
      {path: '/admin/tag', component: () => import('@/views/admin/TagList')},
      //{path: '/admin/dept', component: () => import('@/views/admin/dept/index')},
      {path: '/admin/comment', component: () => import('@/views/admin/CommentList')},
      {path: '/', component: () => import('@/views/admin/Home')},
    ]
  },
  {path: '/login', component: () => import('@/views/Login.vue')},

  // 动态路径参数，以冒号开头
  {path: '/item/:id', component: () => import('@/views/index/Item.vue')},
  /*其他页面*/
  { // 链接对象
    path: '/', // 链接路径
    name: 'HelloWord', // 路由名称
    component: HelloWorld // 对应的组件模板
    // component: '@/components/HelloWorld' // 引入根目录下的 HelloWorld.vue 组件 // 对应的组件模板
  }
]

const router = new Router({
  mode: 'history',
  routes: constantRouterMap // 配置路由，这里是个数组
})
export default router
